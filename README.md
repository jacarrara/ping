# Ping

Docker image based on busybox for ICMP ping.

**Usage**
```bash
$ docker run -d jacarrara/ping [options] <IP or FQDN>
```

**List options**

```bash
$ docker run jacarrara/ping
```

**License**

MIT
